from django.db.models.query import QuerySet
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.contrib.auth.views import LoginView, PasswordChangeView
from django.views.generic.edit import FormView, UpdateView
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.contrib.auth import login
from django.shortcuts import redirect
from .models import Movie
from .forms import MovieForm, ReviewForm
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib import messages
from django.shortcuts import render, redirect


import requests
#za api
from django.shortcuts import render, redirect, get_object_or_404
import http.client
import json
from django.http import JsonResponse
from django.views.decorators.http import require_GET
from django.http import JsonResponse
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required
from .models import Movie
import json
from bs4 import BeautifulSoup
from django.views.decorators.csrf import csrf_protect
from django.utils.decorators import method_decorator
#za sve usere
from django.contrib.auth import get_user_model


@method_decorator(csrf_protect, name='dispatch')

#@csrf_protect
class CustomLoginView(LoginView):
    
    template_name = 'movies/login.html'
    fields = '__all__'
    redirect_authenticated_user = True

    def get_success_url(self):
        return reverse_lazy('movie-list')
    
@method_decorator(csrf_protect, name='dispatch')
class RegisterPage(FormView):
    template_name = 'movies/register.html'
    form_class = UserCreationForm
    redirect_authenticated_user = True
    success_url = reverse_lazy('movie-list')

    def form_valid(self, form):
        user = form.save()
        if user is not None:
            login(self.request, user)
        return super(RegisterPage, self).form_valid(form)

    def get(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('movie-list')
        return super(RegisterPage, self).get(*args, **kwargs)

class MovieList(LoginRequiredMixin, ListView):
    login_url = 'login'
    model = Movie
    template_name = 'movies/movie_list.html'
    context_object_name = 'movies'

    def get_queryset(self):
        queryset = Movie.objects.filter(user=self.request.user)
        search_input = self.request.GET.get('search-area') or ''
        if search_input:
            queryset = queryset.filter(title__icontains=search_input)
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['search_input'] = self.request.GET.get('search-area') or ''
        return context
    
class ChangePasswordView(PasswordChangeView):
    form_class=PasswordChangeForm
    success_url = reverse_lazy('movie-list')
    template_name = 'movies/change_password.html'
    model= Movie
    


@method_decorator(staff_member_required, name='dispatch')
class MovieListAdmin(LoginRequiredMixin, ListView):
       

        template_name = 'movies/admin_list.html'
        login_url = 'login'
        model = Movie
        context_object_name = 'movies'

        


        def get_queryset(self):
            queryset = Movie.objects.all().order_by('user')  
            if not self.request.user.is_staff:  
                queryset = queryset.filter(user=self.request.user)  
            search_input = self.request.GET.get('search-area', '')
            if search_input:
                queryset = queryset.filter(title__icontains=search_input)
            return queryset

     
class MovieDetail(LoginRequiredMixin, DetailView):
    model = Movie
    context_object_name = 'movie'
    template_name = 'movies/movie_detail.html'



class AddReviewView(LoginRequiredMixin, UpdateView):
    model = Movie
    form_class = ReviewForm
    template_name = 'movies/add_review.html'
    pk_url_kwarg = 'movie_id'

    def get_success_url(self):
        return reverse_lazy('movie-detail', kwargs={'pk': self.object.id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['movie'] = self.object
        return context
    
class MovieCreate(LoginRequiredMixin, CreateView):
    model = Movie
    form_class = MovieForm  
    template_name = 'movies/add_movie.html'
    success_url = reverse_lazy('movie-list')

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

class MovieUpdate(LoginRequiredMixin, UpdateView):
    model = Movie
    form_class = MovieForm  
    template_name = 'movies/add_movie.html'
    success_url = reverse_lazy('movie-list')


class MovieDelete(LoginRequiredMixin, DeleteView):
    model = Movie
    context_object_name = 'movie'
    success_url = reverse_lazy('movie-list')
    template_name = 'movies/movie_delete.html'




@require_GET
def search_movies(request):
    query = request.GET.get('query', '')
    if not query:
        return JsonResponse({'error': 'No query provided'}, status=400)

    conn = http.client.HTTPSConnection("movie-database-alternative.p.rapidapi.com")
    
    headers = {
        'x-rapidapi-key': "156c2640f1msh5512309a24c7a21p13ee3bjsn984c58f5711d",
        'x-rapidapi-host': "movie-database-alternative.p.rapidapi.com"
    }
    
    conn.request("GET", f"/?s={query}&r=json&page=1", headers=headers)
    
    res = conn.getresponse()
    data = res.read()
    
    return JsonResponse(json.loads(data.decode("utf-8")))


def search_page(request):
    return render(request, 'movies/movies_from_api.html')



#sa beautiful supom

@login_required
@require_POST
def add_movie_from_api(request):
    data = json.loads(request.body)
    title = data.get('title')
    year = data.get('year')
    imdb_id = data.get('imdbID')
    poster = data.get('poster')

    if not imdb_id:
        return JsonResponse({'success': False, 'message': 'IMDB ID je obavezan.'})

   
    url = f"https://www.imdb.com/title/{imdb_id}/"
    response = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
    soup = BeautifulSoup(response.text, 'html.parser')
    
    
    description_element = soup.find('span', {'data-testid': 'plot-xl'})
    description = description_element.text if description_element else "Opis nije dostupan."

    
    if not Movie.objects.filter(imdb_id=imdb_id).exists():
        movie = Movie.objects.create(
            user=request.user,
            title=title,
            release_date=f"{year}-01-01",  
            description=description,
            poster_url=poster,
            imdb_id=imdb_id
        )
        return JsonResponse({'success': True, 'message': 'Film je uspješno dodan.'})
    else:
        return JsonResponse({'success': False, 'message': 'Film već postoji u vašoj kolekciji.'})





