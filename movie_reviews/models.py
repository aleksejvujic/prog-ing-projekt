from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

class Movie(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    description = models.TextField()
    review = models.TextField(blank=True, null = True)
    rating = models.DecimalField(max_digits=3, decimal_places=1,blank=True, null = True)
    release_date = models.DateField()
    imdb_id = models.CharField(max_length=20, unique=True)
    poster_url = models.URLField(blank=True)
    created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['release_date']