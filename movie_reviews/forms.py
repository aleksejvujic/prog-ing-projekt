from django import forms
from .models import Movie
from django.forms.widgets import DateInput


from django.forms import ModelForm




class MovieForm(forms.ModelForm):
    release_date = forms.DateField(
        widget=forms.DateInput(attrs={'type': 'date'}),
        help_text='Kliknite za odabir datuma'
    )

    class Meta:
        model = Movie
        fields = ['title', 'description', 'release_date',  'poster_url', 'imdb_id']
        widgets = {
            'release_date': DateInput(attrs={'type': 'date'}),
        }

class ReviewForm(forms.ModelForm):
    class Meta:
        model = Movie
        fields = ['review', 'rating']
        widgets = {
            'rating': forms.Select(choices=[(i, i) for i in range(1, 6)]),
            'review': forms.Textarea(attrs={'rows': 4}),
        }

class ChangePasswordForm(forms.Form):
    model = Movie
    fields = ['username']
    old_password = forms.CharField(widget=forms.PasswordInput())
    new_password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

