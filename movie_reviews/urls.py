from django.urls import path
from .views import (MovieList, MovieDetail, MovieCreate, MovieUpdate, MovieDelete, 
CustomLoginView, RegisterPage, AddReviewView, 
MovieListAdmin, ChangePasswordView) 

from django.contrib.auth.views import LogoutView
from . import views
from django.contrib.auth import views as auth_views
from django.contrib import admin

#moviie api




urlpatterns = [
    path('admin/', admin.site.urls),

    path('', MovieList.as_view(), name='movie-list'),

    path('login/', CustomLoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(next_page='login'), name='logout'),
    path('register/', RegisterPage.as_view(), name='register'),
    
    path('movie/<int:pk>/', MovieDetail.as_view(), name='movie-detail'),
    path('movie-create/', MovieCreate.as_view(), name='movie-create'),
    path('movie-update/<int:pk>/', MovieUpdate.as_view(), name='movie-update'),
    path('movie-delete/<int:pk>/', MovieDelete.as_view(), name='movie-delete'),
    path('add/', MovieCreate.as_view(), name='add_movie'),  # Dodajte ovu liniju
    #api
    path('search/', views.search_movies, name='search_movies'),
    path('search-page/', views.search_page, name='search_page'),
    path('add-movie-from-api/', views.add_movie_from_api, name='add_movie_from_api'),
    path('movie/<int:movie_id>/', MovieDetail.as_view(), name='movie-detail'),
    path('movie/<int:movie_id>/add_review/', AddReviewView.as_view(), name='add-review'),


    path('admin-list', MovieListAdmin.as_view(), name='admin-list'),

    #lozinka
    path('change-password/', ChangePasswordView.as_view(), name='change_password'),
    #path('update-user/', ChangeUserName.as_view(), name='update_user'),





]