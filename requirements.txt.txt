Projektna dokumentacija
aplikacije "Moji Filmovi"
Opis aplikacije:
Aplikacija "Moji Filmovi" omogućuje korisnicima pregled, ocjenjivanje i komentiranje filmova. Korisnici mogu dodavati nove filmove u svoju kolekciju, pretraživati filmove putem vanjskog API-ja, te dijeliti svoje mišljenje o filmovima kroz recenzije i ocjene.
Korisnički zahtjevi
1.1 Registracija korisnika
Req U1 - Web aplikacija će omogućiti korisniku da se registrira
Web aplikacija će omogućiti korisniku registraciju korisničkog računa. Korisnik će unijeti svoje korisničko ime, email adresu i lozinku. Web aplikacija će provjeriti jedinstvenost korisničkog imena i email adrese, te neće dopustiti registraciju ukoliko već postoji korisnik s tim podacima.
1.2 Korisnička prijava
Req U2 - Web aplikacija će omogućiti korisniku prijavu u svoj korisnički račun
Web aplikacija će omogućiti korisniku prijavu u njegov korisnički račun. Korisnik će unijeti svoje korisničko ime i lozinku. Web aplikacija će provjeriti podudaraju li se uneseni podaci s postojećima u bazi podataka.
1.3 Odjava korisnika
Req U3 - Web aplikacija će omogućiti korisnicima da se odjave s njihovog korisničkog računa
Web aplikacija će omogućiti korisniku da se može odjaviti sa svojeg računa. Korisnik će pritisnuti na "Odjavi se" gumb u navigaciji. Aplikacija će poništiti korisničku sesiju.
1.4 Pregled filmova
Req U4 - Web aplikacija će omogućiti korisniku pregled svoje kolekcije filmova
Web aplikacija će omogućiti korisniku uvid u listu filmova koje je dodao u svoju kolekciju. Lista će sadržavati osnovne informacije o filmovima kao što su naslov, godina izdanja i poster.
1.5 Dodavanje filma
Req U5 - Web aplikacija će omogućiti korisniku dodavanje novih filmova u svoju kolekciju
Web aplikacija će pružiti korisniku mogućnost dodavanja novih filmova u svoju kolekciju. Korisnik će moći unijeti informacije o filmu kao što su naslov, opis, datum izlaska i poster.
1.6 Pretraživanje filmova putem API-ja
Req U6 - Web aplikacija će omogućiti korisnicima pretraživanje filmova putem vanjskog API-ja
Web aplikacija će omogućiti korisniku pretraživanje filmova koristeći vanjski API. Korisnik će moći unijeti naziv filma, a aplikacija će prikazati rezultate pretrage s osnovnim informacijama o filmovima.
1.7 Dodavanje filma iz API rezultata
Req U7 - Web aplikacija će omogućiti korisniku dodavanje filma iz rezultata API pretrage u svoju kolekciju
Web aplikacija će omogućiti korisniku da odabere film iz rezultata API pretrage i doda ga u svoju kolekciju. Aplikacija će automatski popuniti informacije o filmu na temelju podataka dobivenih iz API-ja.
1.8 Ocjenjivanje filmova
Req U8 - Web aplikacija će omogućiti korisniku ocjenjivanje filmova
Web aplikacija će omogućiti korisnicima ocjenjivanje filmova u svojoj kolekciji. Korisnik će moći dodijeliti ocjenu filmu na skali od 1 do 5.
1.9 Komentiranje filmova
Req U9 - Web aplikacija će omogućiti korisniku komentiranje filmova
Web aplikacija će omogućiti korisnicima dodavanje komentara uz filmove u svojoj kolekciji. Korisnik će moći unijeti tekstualni komentar koji će biti prikazan uz informacije o filmu.
1.10 Uređivanje informacija o filmu
Req U10 - Web aplikacija će omogućiti korisniku uređivanje informacija o filmovima u svojoj kolekciji
Web aplikacija će omogućiti korisniku da uredi informacije o filmovima koje je dodao u svoju kolekciju. Korisnik će moći promijeniti naslov, opis, datum izlaska i poster filma.
2.1 Zahtjevi Admina
Zahtjevi admina stavljaju fokus na specifične funkcionalnosti koje omogućuju nadzor i uređivanje svih podataka registriranih korisnika i filmova u sustavu.
2.2 Adminsko korisničko sučelje
Req A1 - Web aplikacija će prikazati administratorsko sučelje
Web aplikacija će prikazati administratorsko sučelje koje je dostupno samo administratoru. Administratorsko sučelje će koristiti Django Admin interface. Administratorsko sučelje će omogućiti:

Pregled i uređivanje svih korisničkih podataka
Pregled, uređivanje i brisanje svih filmova u sustavu
Pregled i moderiranje komentara i ocjena

2.3 Upravljanje korisnicima
Req A2 - Web aplikacija će omogućiti adminu upravljanje korisničkim računima
Web aplikacija će omogućiti adminu da pregledava, uređuje i briše korisničke račune. Admin će moći promijeniti korisničke podatke, resetirati lozinke i deaktivirati račune ako je potrebno.
2.4 Upravljanje filmovima
Req A3 - Web aplikacija će omogućiti adminu upravljanje svim filmovima u sustavu
Web aplikacija će omogućiti adminu da pregledava, uređuje i briše sve filmove u sustavu, neovisno o tome koji ih je korisnik dodao. Admin će moći ispravljati netočne informacije o filmovima i uklanjati neprimjeren sadržaj.